%module pImg
%{
#define SWIG_FILE_WITH_INIT
#include "pImg.h"
#include "pImg.cpp"
%}

%include "numpy.i"

%init %{
import_array();
%}


template<typename PixelType,unsigned int Dim>
class pImg {
public:
    typedef double CoordType;

    // constructor/destructor
    pImg();
    ~pImg();

    // python interface inputs
    void SetScalars(PixelType* scalars, int sz1, int sz2, int sz3);
    void SetInputSpacing(CoordType* spacing, int sz);
    void SetInputOrigin(CoordType* origin, int sz);

    // python interface outputs
    void CopyOutput(PixelType* scalars_cpout, int sz1, int sz2, int sz3);
    void GetOutputPtr(PixelType** scalars_out, int* sz1, int* sz2, int* sz3);
    void GetOutputSpacing(CoordType spacing_out[3]);
    void GetOutputOrigin(CoordType origin_out[3]);

    // example
    void MagGrad();
};

#typedef unsigned short ushort;

%define TEMPLATE_HELPER(PixType,ImgDim)
%template(pImg_ ## PixType ## ImgDim) pImg<PixType,ImgDim>;
%apply (PixType * IN_ARRAY3, int DIM1, int DIM2, int DIM3){(PixType * scalars, int sz1, int sz2, int sz3)}
%apply (PixType * INPLACE_ARRAY3, int DIM1, int DIM2, int DIM3){(PixType * scalars_cpout, int sz1, int sz2, int sz3)}
%apply (PixType ** ARGOUTVIEW_ARRAY3, int* DIM1, int* DIM2, int* DIM3){(PixType ** scalars_out, int* sz1, int* sz2, int* sz3)}
%enddef

// SWIG macro expansion to force instantiation of relevant types, currently only 2D/3D images supported
// Comment out irrelevant types for faster compiling
TEMPLATE_HELPER(short,2)
TEMPLATE_HELPER(short,3)
TEMPLATE_HELPER(ushort,2)
TEMPLATE_HELPER(ushort,3)
TEMPLATE_HELPER(float,2)
TEMPLATE_HELPER(float,3)
TEMPLATE_HELPER(double,2)
TEMPLATE_HELPER(double,3)

%apply (double* IN_ARRAY1, int DIM1) {(double* spacing, int sz),(double* origin, int sz)}

// Generic typemap for outputting on return an array of doubles
%typemap(in,numinputs=0) double[ANY] (double temp[$1_dim0]) {
    memset(temp, 0, sizeof temp);
    $1 = temp;
}
%typemap(argout) double[ANY] {
    int i;
    $result = PyList_New($1_dim0);
    for (i = 0; i < $1_dim0; i++) {
        PyObject *o = PyFloat_FromDouble((double) temp$argnum[i]);
        PyList_SetItem($result,i,o);
    }
}

